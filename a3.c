#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/mman.h>


typedef struct {
   char name[15];
   unsigned int type;
   unsigned int offset;
   unsigned int size;
   unsigned int pageNoBegin;
} SECTION;

SECTION sect[20];

unsigned int NO = 71817;
char c5 = 5;
char c7 = 7;
char c8 = 8;

int fdWr;
int fdRd;
int fdSm;
char *smAdd;
char *mapAdd;
struct stat elem;
unsigned int dim;
int msg_size = 0;

void pingPong();
void createSHM();
void writeToSHM();
void mapFile();
void readFileOffset();
void readSectionHeaders(unsigned int numberOfSections, unsigned int currentPos);
void readFileSection();
void readLogicalOffset();

int main(int argc, char *argv[])
{
    if (mkfifo("RESP_PIPE_71817", 0644) < 0)
    {
        printf("ERROR\ncannot create the response pipe");
        return -1;
    }
    fdWr = open("RESP_PIPE_71817", O_RDWR);

    if ((fdRd = open("REQ_PIPE_71817", O_RDWR)) < 0)
    {
        printf("ERROR\ncannot open the request pipe");
        return -1;
    }

    write(fdWr, &c7, 1);
    write(fdWr, "CONNECT", 7);
    printf("SUCCESS");

    msg_size = 0;
    char *cmd;
    read(fdRd, &msg_size, 1);
    cmd = (char*)malloc(msg_size + 1);
    read(fdRd, cmd, msg_size);
    cmd[msg_size] = '\0';

    while(strcmp(cmd, "EXIT") != 0)
    {

        if(strcmp(cmd, "PING") == 0)
            pingPong();

        if(strcmp(cmd, "CREATE_SHM") == 0)
            createSHM();

        if(strcmp(cmd, "WRITE_TO_SHM") == 0)
            writeToSHM();

        if(strcmp(cmd, "MAP_FILE") == 0)
            mapFile();

        if(strcmp(cmd, "READ_FROM_FILE_OFFSET") == 0)
            readFileOffset();

        if(strcmp(cmd, "READ_FROM_FILE_SECTION") == 0)
            readFileSection();

        if(strcmp(cmd, "READ_FROM_LOGICAL_SPACE_OFFSET") == 0)
            readLogicalOffset();

        read(fdRd, &msg_size, 1);
        cmd = (char*)malloc(msg_size + 1);
        read(fdRd, cmd, msg_size);
        cmd[msg_size] = '\0';
    }
    shmdt(smAdd);
    close(fdRd);
    close(fdWr);
}

void pingPong()
{
    write(fdWr, &msg_size, 1);
    write(fdWr, "PING", 4);
    write(fdWr, &msg_size, 1);
    write(fdWr, "PONG", 4);
    write(fdWr, &NO, sizeof(unsigned int));
}

void createSHM()
{
    read(fdRd, &dim, sizeof(unsigned int));
    write(fdWr, &msg_size, 1);
    write(fdWr, "CREATE_SHM", 10);
    if((fdSm = shmget(10599, dim, IPC_CREAT | 0644)) < 0)
    {
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        if((smAdd = shmat(fdSm, NULL, SHM_RND)) < 0)
        {
            write(fdWr, &c5, 1);
            write(fdWr, "ERROR", 5);
        }
        else
        {
            write(fdWr, &c7, 1);
            write(fdWr, "SUCCESS", 7);
        }
    }
}

void writeToSHM()
{
    unsigned int offset;
    char value[sizeof(unsigned int)];
    read(fdRd, &offset, sizeof(unsigned int));
    for(int i = 0; i < sizeof(unsigned int); i++)
        read(fdRd, &value[i], 1);

    write(fdWr, &msg_size, 1);
    write(fdWr, "WRITE_TO_SHM", 12);
    if(offset + sizeof(unsigned int) > dim)
    {
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        for(int i = 0; i < sizeof(unsigned int); i++)
            smAdd[offset + i] = value[i];

        write(fdWr, &c7, 1);
        write(fdWr, "SUCCESS", 7);
    }
}

void mapFile()
{
    read(fdRd, &msg_size, 1);
    char *cmd = (char*)malloc(msg_size + 1);
    read(fdRd, cmd, msg_size);
    cmd[msg_size] = '\0';
    write(fdWr, &c8, 1);
    write(fdWr, "MAP_FILE", 8);
    int fd = open(cmd, O_RDONLY);
    if(fd < 0)
    {
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        lstat(cmd, &elem);
        int size = elem.st_size;
        mapAdd = mmap(0, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE, fd, 0);
        if(mapAdd < 0)
        {
            write(fdWr, &c5, 1);
            write(fdWr, "ERROR", 5);
        }
        else
        {
            write(fdWr, &c7, 1);
            write(fdWr, "SUCCESS", 7);
        }
    }
}

void readFileOffset()
{
    unsigned int offset;
    unsigned int bytesNo;
    read(fdRd, &offset, sizeof(unsigned int));
    read(fdRd, &bytesNo, sizeof(unsigned int));
    if((smAdd < 0) | (mapAdd < 0) | (offset + sizeof(unsigned int) > elem.st_size))
    {
        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_FILE_OFFSET", 21);
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        for(int i = 0; i < bytesNo; i++)
            smAdd[i] = mapAdd[offset + i];

        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_FILE_OFFSET", 21);
        write(fdWr, &c7, 1);
        write(fdWr, "SUCCESS", 7);
    }

}

void readSectionHeaders(unsigned int numberOfSections, unsigned int currentPos)
{
    currentPos++;
    unsigned int currentPage = 0;
    for(int i = 0; i < numberOfSections; i++)
    {
        sect[i].pageNoBegin = currentPage;
        for(int j = 0; j < 14; j++)
        {
            sect[i].name[j] = mapAdd[currentPos];
            currentPos++;
        }
        sect[i].name[14] = '\0';
        sect[i].type = ((mapAdd[currentPos + 3] << 24) & 0xFF000000) + ((mapAdd[currentPos + 2] << 16) & 0x00FF0000) + ((mapAdd[currentPos + 1] << 8) & 0x0000FF00) + (mapAdd[currentPos] & 0x000000FF);
        currentPos += 4;
        sect[i].offset = ((mapAdd[currentPos + 3] << 24) & 0xFF000000) + ((mapAdd[currentPos + 2] << 16) & 0x00FF0000) + ((mapAdd[currentPos + 1] << 8) & 0x0000FF00) + (mapAdd[currentPos] & 0x000000FF);
        currentPos += 4;
        sect[i].size = ((mapAdd[currentPos + 3] << 24) & 0xFF000000) + ((mapAdd[currentPos + 2] << 16) & 0x00FF0000) + ((mapAdd[currentPos + 1] << 8) & 0x0000FF00) + (mapAdd[currentPos] & 0x000000FF);
        currentPos += 4;
        if(sect[i].size % 1024 == 0)
            currentPage += sect[i].size / 1024;
        else
            currentPage += sect[i].size / 1024 + 1;
    }
}

void readFileSection()
{
    unsigned int sectionNo;
    unsigned int offset;
    unsigned int bytesNo;
    read(fdRd, &sectionNo, sizeof(unsigned int));
    read(fdRd, &offset, sizeof(unsigned int));
    read(fdRd, &bytesNo, sizeof(unsigned int));
    sectionNo--;
    unsigned int headerSize = (mapAdd[elem.st_size - 3] << 8) + mapAdd[elem.st_size - 4];
    unsigned int currentPos = elem.st_size - headerSize + 4;
    unsigned int numberOfSections = mapAdd[currentPos];

    readSectionHeaders(numberOfSections, currentPos);

    if((smAdd < 0) || (mapAdd < 0) || (offset + bytesNo >= sect[sectionNo].size) || (sectionNo + 1 > numberOfSections))
    {
        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_FILE_SECTION", 22);
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        for(int i = 0; i < bytesNo; i++)
            smAdd[i] = mapAdd[sect[sectionNo].offset + offset + i];

        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_FILE_SECTION", 22);
        write(fdWr, &c7, 1);
        write(fdWr, "SUCCESS", 7);
    }
}

void readLogicalOffset()
{
    unsigned int offset;
    unsigned int bytesNo;
    read(fdRd, &offset, sizeof(unsigned int));
    read(fdRd, &bytesNo, sizeof(unsigned int));

    unsigned int headerSize = ((mapAdd[elem.st_size - 3] << 8) & 0x0000FF00) + (mapAdd[elem.st_size - 4] & 0x000000FF);
    unsigned int currentPos = elem.st_size - headerSize + 4;
    unsigned int numberOfSections = mapAdd[currentPos];

    readSectionHeaders(numberOfSections, currentPos);

    unsigned int pagesNo = (offset >> 10);
    int i = 0;


    while (sect[i].pageNoBegin <= pagesNo)
        i++;

    i--;
    offset -= sect[i].pageNoBegin * 1024;

    if((smAdd < 0) || (mapAdd < 0) || (offset + bytesNo) > sect[i].size)
    {
        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_LOGICAL_SPACE_OFFSET", 30);
        write(fdWr, &c5, 1);
        write(fdWr, "ERROR", 5);
    }
    else
    {
        for(int j = 0; j < bytesNo; j++)
            smAdd[j] = mapAdd[sect[i].offset + offset + j];

        write(fdWr, &msg_size, 1);
        write(fdWr, "READ_FROM_LOGICAL_SPACE_OFFSET", 30);
        write(fdWr, &c7, 1);
        write(fdWr, "SUCCESS", 7);
    }
}
